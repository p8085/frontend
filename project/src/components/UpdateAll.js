import { useLocation } from "react-router"
import axios from 'axios'
import Home from '../pages/Home'
import { toast } from 'react-toastify'
import { Link, useNavigate } from 'react-router-dom'
import { URL } from '../config'
import { useEffect, useState } from 'react'
const styles = {
    bar: {
        backgoundColor: 'black',
        height: '80px',
        display: 'flex'
    },
    centerContent: {
        textAlign: 'left'
    }
}

const UpdateAll = () => {
    const { state } = useLocation()
    const { records } = state
    console.log(records)
    const [sscScore, setSscScore] = useState(`${records.sscScore}`)
    console.log('sscScore', sscScore)
    const [hscScore, setHscScore] = useState(`${records.hscScore}`)
    const [diplomaScore, setDiplomaScore] = useState(`${records.diplomaScore}`)
    const [firstSem, setFirstSemScore] = useState(`${records.firstSem}`)
    const [secondSem, setSecondSemScore] = useState(`${records.secondSem}`)
    const [thirdSem, setThirdSemScore] = useState(`${records.thirdSem}`)
    const [fourthSem, setFourthSemScore] = useState(`${records.fourthSem}`)
    const [fifthSem, setFifthSemScore] = useState(`${records.fifthSem}`)
    const [sixthSem, setSixthSemScore] = useState(`${records.sixthSem}`)
    const [seventhSem, setSeventhSemScore] = useState(`${records.seventhSem}`)
    const [eigthSem, setEighthSemScore] = useState(`${records.seventhSem}`)
    const [grade, setGrade] = useState(`${records.grade}`)
    const { enrollmentNo, firstName, lastName } = sessionStorage
    const navigate = useNavigate()
    console.log("inside updateall after add",records.student.enrollmentNo)
    const addAllAcademicDetails = () => {
        const body = {
            sscScore,
            hscScore,
            diplomaScore,
            firstSem,
            secondSem,
            thirdSem,
            fourthSem,
            fifthSem,
            sixthSem,
            seventhSem,
            eigthSem,
            grade

        }

        console.log("inside add All academic Records")
        const url = `${URL}/student/updateAllAcademicDetails/${records.student.enrollmentNo}`
        console.log( url )
        axios.patch(url, body).then((response) => {

            const result = response.data
            console.log(result)
            if (result['status'] === 'success') {
                toast.success('Added successfully')

                // get the data sent by server
                const { academiRecords } = result['data']
                console.log('academic:', academiRecords)

                navigate('/getAllRecords')
            } else {
                toast.error('Invalid data')
            }
        })
    }

    return (
        <div>

            <div>
                <div >
                    <Home />
                </div>
                <h1 className="title">EditStudent</h1>

                <div className="row" style={styles.centerContent}>
                    <div className="col"></div>
                    <div className="col">

                        <div className="form">

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    SSC Score
                                </label>
                                <input
                                    onChange={(e) => {
                                        setSscScore(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={sscScore}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    HSC Score
                                </label>
                                <input
                                    onChange={(e) => {
                                        setHscScore(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={hscScore}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Diploma Score
                                </label>
                                <input
                                    onChange={(e) => {
                                        setDiplomaScore(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={diplomaScore}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    First Semester Score
                                </label>
                                <input
                                    onChange={(e) => {
                                        setFirstSemScore(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={firstSem}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Second Semester Score
                                </label>
                                <input
                                    onChange={(e) => {
                                        setSecondSemScore(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={secondSem}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Third Semester Score
                                </label>
                                <input
                                    onChange={(e) => {
                                        setThirdSemScore(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={thirdSem}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Fourth Semester Score
                                </label>
                                <input
                                    onChange={(e) => {
                                        setFourthSemScore(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={fourthSem}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Fifth Semester Score
                                </label>
                                <input
                                    onChange={(e) => {
                                        setFifthSemScore(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={fifthSem}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Address Line2
                                </label>
                                <input
                                    onChange={(e) => {
                                        setSixthSemScore(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={sixthSem}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Seventh Semester Score
                                </label>
                                <input
                                    onChange={(e) => {
                                        setSeventhSemScore(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={seventhSem}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Eigth Semester Score
                                </label>
                                <input
                                    onChange={(e) => {
                                        setEighthSemScore(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={eigthSem}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Grade
                                </label>
                                <input
                                    onChange={(e) => {
                                        setGrade(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={grade}
                                />
                            </div>

                            <div className="mb-3">

                                <button onClick={addAllAcademicDetails} type="button" class="btn btn-secondary btn-lg">Update</button>
                                <Link to="/getAllRecords" className="btn btn-secondary float-end">
                                    Cancel
                                </Link>

                            </div>
                        </div>

                    </div>
                    <div className="col"></div>
                </div>
            </div>
















            {/* <div className="container">
                <div className="row">
                    <div className="col-2"></div>
                    <div className="col">
                        <div className="row">
                            <div className="col"></div>
                            <div className="col">
                                <h5>Add Academic Record</h5>
                            </div>
                            <div className="col"></div>
                        </div>

                        <div className="row">
                            <div className="col">
                                <div class="form-floating mb-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="floatingInput4"
                                        onChange={(e) => {
                                            setSscScore(e.target.value)
                                        }}
                                        value={sscScore}
                                    />
                                    <label for="floatingInput">SSC Score</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div class="form-floating mb-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="floatingInput4"
                                        onChange={(e) => {
                                            setHscScore(e.target.value)
                                        }}
                                        value={hscScore}
                                    />
                                    <label for="floatingInput">HSC Score</label>
                                </div>
                            </div>
                        </div>
                        
                        <div className="row">
                            <div className="col">
                                <div class="form-floating mb-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="floatingInput4"
                                        onChange={(e) => {
                                            setDiplomaScore(e.target.value)
                                        }}
                                        value={diplomaScore}
                                    />
                                    <label for="floatingInput">Diploma Score</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div class="form-floating mb-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="floatingInput4"
                                        onChange={(e) => {
                                            setFirstSemScore(e.target.value)
                                        }}
                                        value={diplomaScore}
                                    />
                                    <label for="floatingInput">First Semester Score</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div class="form-floating mb-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="floatingInput"
                                        onChange={(e) => {
                                            setSecondSemScore(e.target.value)
                                        }}
                                        value={secondSem}
                                        
                                    />
                                    
                                    <label for="floatingInput">Second Semester Score</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div class="form-floating mb-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="floatingInput"
                                        onChange={(e) => {
                                            setThirdSemScore(e.target.value)
                                        }}
                                        value={thirdSem}
                                    />
                                    <label for="floatingInput">Third Semester Score</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div class="form-floating mb-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="floatingInput"
                                        onChange={(e) => {
                                            setFourthSemScore(e.target.value)
                                        }}
                                        value={fourthSem}
                                    />
                                    <label for="floatingInput">Fourth Semester Score</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div class="form-floating mb-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="floatingInput"
                                        onChange={(e) => {
                                            setFifthSemScore(e.target.value)
                                        }}
                                        value={fifthSem}
                                    />
                                    <label for="floatingInput">Fifth Semester Score</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div class="form-floating mb-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="floatingInput"
                                        onChange={(e) => {
                                            setSixthSemScore(e.target.value)
                                        }}
                                        value={sixthSem}
                                    />
                                    <label for="floatingInput">Sixth Semester Score</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div class="form-floating mb-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="floatingInput"
                                        onChange={(e) => {
                                            setSeventhSemScore(e.target.value)
                                        }}
                                        value={seventhSem}
                                    />
                                    <label for="floatingInput">Seventh Semester Score</label>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div class="form-floating mb-3">
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="floatingInput"
                                        onChange={(e) => {
                                            setEighthSemScore(e.target.value)
                                        }}
                                        value={eigthSem}
                                    />
                                    <label for="floatingInput">Eigth Semester Score</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <button onClick={addAllAcademicDetails} type="button" class="btn btn-secondary btn-lg">Add</button>
                    </div>
                </div>
            </div> */}
        </div>
    )
}

export default UpdateAll