
import { useNavigate } from 'react-router'
import { Accordion } from 'react-bootstrap'
const styles = {
    div: {
        float: 'right',
        width: '100%',
    },
    body: {
        testAlign: 'left'
    },
    toggleButton: {
        cursor: 'pointer',
        width: '20px',
        height: '20px',
        margin: '5px',
      },

}
const DisplayRecords = (props) => {

    const navigate = useNavigate()
    const { records } = props
    console.log("stude:",records.student.enrollmentNo)
    console.log("firstSem",records.firstSem)
    return (
        <div>
            <div className="row">
            <div className="col"></div>
            <div className="col">
            <div class="accordion" id="accordionPanelsStayOpenExample" style={styles.div}>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                       
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                        Academic record for Enrollment No :{records.student.enrollmentNo}
                        
                        </button>
                    </h2>
                    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                    <div class="accordion-body">SSC Score : {records.sscScore}   HSC Score : {records.hscScore}  Diploma Score : {records.diplomaScore}  First Semester : {records.firstSem}  Second Semester : {records.secondSem}  Third Semester : {records.thirdSem}  Fourth Semester : {records.fourthSem}  Fifth Semester : {records.fifthSem}  Sixth Semester : {records.sixthSem}  Seventh Semester : {records.seventhSem}  Eigth Semester : {records.eighthSem}</div>
                    </div>
                    
                </div>
            </div>
            </div>
            <div className="col">
            <img
                 style={styles.toggleButton}
                  onClick={() => {
                    navigate('/updateAll', { state: { records: records } })
                  }}
                  src={require('../assets/Edit.png')}
                ></img>
            </div>
            </div>
            

            
            

            {/* <div class="accordion accordion-flush" id="accordionFlushExample" style={styles.div}>
                <div class="accordion-item">
                    <h1 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            Enrollment No :{records.student.enrollmentNo}
                        </button>

                    </h1>
                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body" >First Name: {records.student.firstName}</div>
                        <div class="accordion-body" >Last Name : {records.student.lastName}</div>
                        <div class="accordion-body" >email : {records.student.email}</div>
                    </div>
                    </div>
                    <div class="accordion-item">
                    <h3 class="accordion-header" id="flush-headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                            Academic Details
                        </button>
                    </h3>
                    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body" >SSC Score : {records.sscScore}</div>
                        <div class="accordion-body" >HSC Score : {records.hscScore}</div>
                        <div class="accordion-body" >Diploma Score : {records.diplomaScore}</div>
                    </div>
                </div>
                
                
            </div> */}

        </div>

    )
}

export default DisplayRecords