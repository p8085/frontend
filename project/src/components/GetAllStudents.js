import { useEffect, useState } from 'react'
import axios from 'axios'
import { URL } from '../config'
import DisplayStudents from './DisplayStudents'
import Home from '../pages/Home'
const styles={
    table:{
        margin : '20px',
        padding: '20px',
        width : '60%',
        marginLeft : 'auto',
        marginRight:'auto',
        marginTop:'50px'
    },
    div:{
        width : '20%'
    }

}

const GetAllStudents=()=>{

    const [ getStudents,setStudents ] = useState([])
    
    const allStudents=()=>{
        const url=`${URL}/student/findAllStudents`
        axios.get(url).then((response) => {
            const result = response.data
            console.log('resultr',{result})
            if (result['status'] == 'success') {
              setStudents(result['data'])
            }
          })
    }
    useEffect(()=>{
        allStudents()
    },[])
    
    
    return(
        <div>   
            <div> 
                <Home />
            </div>
            <div>
            {getStudents.map((stud) => {
                return <DisplayStudents students={stud}/>
            })}
            
            </div>
            
        </div>
    )
}

export default GetAllStudents