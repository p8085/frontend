import Home from "../../pages/Home"
import { useLocation } from "react-router"
import axios from 'axios'
import { URL } from '../../config'
import { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
const styles = {
    div: {
        width: '857px',
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    cancelButton: {
        display: 'flex',
        justifyContent: 'center',
    alignItems: 'center',
    height: '200px', 
    }
}
const SearchById = () => {
    const [getCompanyDetails, setCompanyDetails] = useState()
    const { state } = useLocation()

    const { id } = state
    console.log('id:', id)

    const searchCompanyById = () => {
        console.log('inside searchByID')
        const url = `${URL}/companydetails/${id}`
        console.log(url)
        axios.get(url).then((response) => {
            const result = response.data
            console.log('result', { result })
            if (result['status'] == 'success') {
                setCompanyDetails(result['data'])

            }
        })
    }

    useEffect(() => {
        searchCompanyById()
    }, [])

    return (
        <div>
            <div>
                <Home />
            </div>
            {getCompanyDetails && (<div style={styles.div}>
                {console.log(getCompanyDetails)}

                <table class="table table-striped" >

                    <tr>
                        <td>CompanyId</td>
                        <td>{getCompanyDetails.companyId}</td>
                    </tr>
                    <tr>
                        <td>Company Name</td>
                        <td>{getCompanyDetails.companyName}</td>
                    </tr>
                    <tr>
                        <td>Company Type</td>
                        <td>{getCompanyDetails.companyType}</td>
                    </tr>
                    <tr>
                        <td>Vacancies</td>
                        <td>{getCompanyDetails.vacancies}</td>
                    </tr>
                    <tr>
                        <td>Company Ctc</td>
                        <td>{getCompanyDetails.companyCtc}</td>
                    </tr>

                </table>
            </div>)}

            <div style={styles.cancelButton} >
                <Link to="/placement" className="btn btn-secondary float-end">
                    Cancel
                </Link>
            </div>

        </div>

    )
}

export default SearchById