
import { useNavigate } from 'react-router'
import { Accordion } from 'react-bootstrap'
const styles = {
    div: {
        float: 'right',
        width: '100%',
    },
    body: {
        testAlign: 'left'
    },
    toggleButton: {
        cursor: 'pointer',
        width: '20px',
        height: '20px',
        margin: '5px',
      },

}
const DisplayCompany = (props) => {

    const navigate = useNavigate()
    const { companyDetails } = props
    
    return (
        <div>
            <div className="row">
            <div className="col"></div>
            <div className="col">
            <div class="accordion" id="accordionPanelsStayOpenExample" style={styles.div}>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
                       
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                        Company Id :{companyDetails.companyId}
                        
                        </button>
                    </h2>
                    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
                    <div class="accordion-body">Company Name : {companyDetails.companyName}  </div>
                    </div>
                    
                </div>
            </div>
            </div>
            <div className="col">
           
            </div>
            </div>
            

            
            

            {/* <div class="accordion accordion-flush" id="accordionFlushExample" style={styles.div}>
                <div class="accordion-item">
                    <h1 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            Enrollment No :{records.student.enrollmentNo}
                        </button>

                    </h1>
                    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body" >First Name: {records.student.firstName}</div>
                        <div class="accordion-body" >Last Name : {records.student.lastName}</div>
                        <div class="accordion-body" >email : {records.student.email}</div>
                    </div>
                    </div>
                    <div class="accordion-item">
                    <h3 class="accordion-header" id="flush-headingTwo">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                            Academic Details
                        </button>
                    </h3>
                    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body" >SSC Score : {records.sscScore}</div>
                        <div class="accordion-body" >HSC Score : {records.hscScore}</div>
                        <div class="accordion-body" >Diploma Score : {records.diplomaScore}</div>
                    </div>
                </div>
                
                
            </div> */}

        </div>

    )
}

export default DisplayCompany