import Home from "../../pages/Home"
import { useLocation } from "react-router"
import axios from 'axios'
import { URL } from '../../config'
import { useEffect, useState } from 'react'
const styles = {
    div: {
        width: '857px',
        marginLeft: 'auto',
        marginRight: 'auto'
    }
}
const SearchByName = () => {
    const [ getCompanyByName, setCompanyByName ]  = useState()
    const { state } = useLocation()
    
    
    
    const searchCompanyByName = () => {
        const { id } = state
        const { companyName } = id
        console.log('inside searchByID')
        const body = {
            placement : {companyName},
        }
        const url = `${URL}/companydetails/byname`
        console.log(url)
         axios.get(url,body).then((response) => {
             console.log("body:",body)
            const result = response.data
            console.log('result', { result })
            if (result['status'] == 'success') {
                setCompanyByName(result['data'])
                
                
            }
        })
    }
    
    useEffect(() => {
        searchCompanyByName()
    }, [])

    return (
        <div>
            <div>
                <Home />
            </div>
            {getCompanyByName  && (<div style={styles.div}>
                

                <table class="table table-striped" >

                    <tr>
                        <td>CompanyId</td>
                        <td>{getCompanyByName.companyId}</td>
                    </tr>
                    <tr>
                        <td>Company Name</td>
                        <td>{getCompanyByName.companyName}</td>
                    </tr>
                    <tr>
                        <td>Company Type</td>
                        <td>{getCompanyByName.companyType}</td>
                    </tr>
                    <tr>
                        <td>Vacancies</td>
                        <td>{getCompanyByName.vacancies}</td>
                    </tr>
                    <tr>
                        <td>Company Ctc</td>
                        <td>{getCompanyByName.companyCtc}</td>
                    </tr>

                </table>
            </div>)}
            


        </div>

    )
}

export default SearchByName