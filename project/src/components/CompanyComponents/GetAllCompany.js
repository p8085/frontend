import Home from "../../pages/Home"
import { useLocation } from "react-router"
import axios from 'axios'
import { URL } from '../../config'
import { useEffect, useState } from 'react'
import DisplayCompany from './DisplayCompany'

const GetAllCompany=()=>{

    

    const [getCompany, setCompany] = useState([])

    const getAllCompany = () => {
        const url = `${URL}/allcompanylist`
        console.log(url)
        axios.get(url).then((response) => {
            const result = response.data
            console.log('result', { result })
            if (result['status'] == 'success') {
                setCompany(result['data'])
            }
        })
    }

    useEffect(() => {
        getAllCompany()
    }, [])

    return (
        <div>
            
            {getCompany.map((company) => {
                {console.log('inside map')}
                return <DisplayCompany companyDetails={company}/>
            })}
        </div>
    )






    
    
}
export default GetAllCompany