import { useLocation } from "react-router"
import axios from 'axios'
import Home from '../pages/Home'
import { toast } from 'react-toastify'
import { Link, useNavigate } from 'react-router-dom'
import { URL } from '../config'
import { useEffect, useState } from 'react'

const UpdateStudent = () => {
    const { state } = useLocation()
    const navigate = useNavigate()
    const { students } = state
        console.log(students)
    const [firstName, setFirst] = useState(`${students.firstName}`)
    const [lastName, setLastName] = useState(`${students.lastName}`)
    const [email, setEmail] = useState(`${students.email}`)
    const [password, setPassword] = useState(`${students.password}`)
    const [middleName, setMiddleName] = useState(`${students.middleName}`)
    const [contactNo, setContactNo] = useState(`${students.contactNo}`)
    const [addressLine1, setAddressLine1] = useState(`${students.addressLine1}`)
    const [addressLine2, setAddressLine2] = useState(`${students.addressLine2}`)
    const [zipId, setZipId] = useState(`${students.zipId}`)
    const [dateOfBirth, setDateOfBirth] = useState(`${students.dateOfBirth}`)


    console.log(firstName)
    const styles = {
        bar: {
            backgoundColor: 'black',
            height: '80px',
            display: 'flex'
        },
        centerContent: {
            textAlign: 'left'
        }
    }

    const updateStudent = () => {
        const body = {
            firstName,
            lastName,
           email,
            password,
            middleName,
            contactNo,
            addressLine1,
            addressLine2,
            zipId,
           dateOfBirth
          }
        const url = `${URL}/student/editStudentDetails/${students.enrollmentNo}`
        console.log(url)
        axios.patch(url,body).then((response) => {
            const result = response.data
            if (result['status'] == 'success') {
                toast.success('successfully deleted student..')
                navigate('/getAllStudents')
            } else {
                toast.error(result['error'])
            }
        })
        console.log(url)
    }
    return (
        <div>
            <div>
                < Home />
            </div>
            <div>
                <div className="row" style={styles.centerContent}>
                    <div className="col"></div>
                    <div className="col">

                        <div className="form">

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    First Name
                                </label>
                                <input
                                    onChange={(e) => {
                                        setFirst(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={firstName}
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Middle Name
                                </label>
                                <input
                                    onChange={(e) => {
                                        setMiddleName(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={middleName}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Last Name
                                </label>
                                <input
                                    onChange={(e) => {
                                        setLastName(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={lastName}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Date of Birth
                                </label>
                                <input
                                    onChange={(e) => {
                                        setDateOfBirth(e.target.value)
                                    }}
                                    type="date"
                                    className="form-control"
                                    placeholder={dateOfBirth}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Email Address
                                </label>
                                <input
                                    onChange={(e) => {
                                        setEmail(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={email}
                                    readOnly
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Password
                                </label>
                                <input
                                    onChange={(e) => {
                                        setPassword(e.target.value)
                                    }}
                                    type="password"
                                    className="form-control"
                                    placeholder='********'
                                    readOnly
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Contact NO
                                </label>
                                <input
                                    onChange={(e) => {
                                        setContactNo(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={contactNo}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Address Line1
                                </label>
                                <input
                                    onChange={(e) => {
                                        setAddressLine1(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={addressLine1}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Address Line2
                                </label>
                                <input
                                    onChange={(e) => {
                                        setAddressLine2(e.target.value)
                                    }}
                                    type="text"
                                    className="form-control"
                                    placeholder={addressLine2}
                                />
                            </div>

                            <div className="mb-3">
                                <label htmlFor="" className="label-control">
                                    Zip Id
                                </label>
                                <input
                                    onChange={(e) => {
                                        setZipId(e.target.value)
                                    }}
                                    type="number"
                                    className="form-control"
                                    placeholder={zipId}
                                />
                            </div>

                            <div className="mb-3">

                                <button onClick={updateStudent} className="btn btn-secondary float-start">
                                    Save
                                </button>
                                <Link to="/getAllStudents" className="btn btn-secondary float-end">
                                    Cancel
                                </Link>

                            </div>
                        </div>

                    </div>
                    <div className="col"></div>
                </div>
            </div>

        </div>
    )
}

export default UpdateStudent