import { useLocation } from "react-router"
import axios from 'axios'
import Home from '../pages/Home'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router'
import { URL } from '../config'
const DeleteStudent = () => {
  const { state } = useLocation()

  const navigate = useNavigate()
  const deleteStudent = () => {
    const { id } = state
    const url = `${URL}/student/deleteStudent/${id}`
    console.log(url)
    axios.delete(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        toast.success('successfully deleted student..')
        navigate('/getAllStudents')
      } else {
        toast.error(result['error'])
      }
    })
    console.log(url)
  }
  return (
    <div>
      <div>
        <Home />
      </div>
      <div>
        <h4>Are you sure you want to delete the student?</h4>
      </div>
      <div>
        <button onClick={deleteStudent}>Confirm Delete</button>
      </div>
    </div>
  )
}

export default DeleteStudent