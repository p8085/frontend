import Signin from '../pages/Signin'

const Logout=()=>
{
    sessionStorage.removeItem('id')
    sessionStorage.removeItem('firstName')
    sessionStorage.removeItem('lastName')
    sessionStorage.removeItem('loginStatus')
    return (
        <div className="Logout">
            <Signin />
        </div>
    )
}

export default Logout;