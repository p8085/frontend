import { useEffect, useState } from 'react'
import axios from 'axios'
import { URL } from '../config'
import DisplayRecords from './DisplayRecords'
import Home from '../pages/Home'


const GetAllRecords = () => {

    const [getRecords, setRecords] = useState([])

    const allRecords = () => {
        const url = `${URL}/student/academicRecords`
        console.log(url)
        axios.get(url).then((response) => {
            const result = response.data
            console.log('result', { result })
            if (result['status'] == 'success') {
                setRecords(result['data'])
            }
        })
    }

    useEffect(() => {
        allRecords()
    }, [])

    return (
        <div>
            <div>
                <Home />
            </div>
            {getRecords.map((rec) => {
                {console.log('inside map')}
                return <DisplayRecords records={rec}/>
            })}
        </div>
    )
}

export default GetAllRecords