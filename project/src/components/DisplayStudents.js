import DeleteStudent from './DeleteStudent'
import axios from 'axios'
import { URL } from '../config'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router'
import UpdateStudent from '../components/UpdateStudent'
const styles={
    table:{
        margin : '20px',
        padding: '20px',
        width : '60%',
        marginLeft : 'auto',
        marginRight:'auto',
        marginTop:'50px'
    },
    div:{
        width : '20%'
    }

}

const DisplayStudents = (props) => {

    const navigate = useNavigate()
    const { students } = props

    
    return (
        <div>
            <div>   
                
            </div>
            <table class="table table-hover" style={styles.table}>
                
                <tbody>
                    
                    <tr class="table-active">
                        <td>{students.enrollmentNo}</td>
                        <td>{students.firstName}</td>
                        <td>{students.lastName}</td>
                        <td>{students.email}</td>
                        <td >
                            
                            <button onClick={()=>{
                                console.log(students.enrollmentNo)
                                navigate('/delete-student',{state : {id : students.enrollmentNo}})

                            }}
              className="btn btn-secondary float-end">Delete</button>
                        </td>
                        <td >
                            
                            <button onClick={()=>{
                                console.log(students.enrollmentNo)
                                navigate('/update-student',{state : {students : students}})

                            }}
              className="btn btn-secondary float-end">Update</button>
                        </td>
                        
                    </tr>
                    
                    
                </tbody>
            </table>
          
        </div>
    )
}

export default DisplayStudents