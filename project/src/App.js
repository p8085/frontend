import logo from './logo.svg';
import './App.css';
import Signin from './pages/Signin'
import Home from './pages/Home'
import PersonalDetails from './components/PersonalDetails'
import AcademicDetails from './components/AcademicDetails'
import { BrowserRouter, Route, Routes, Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Logout from './components/Logout';
import Faq from './components/Faq';
import EditDetails from './components/EditDetails';
import EditAcademicDetails from './components/EditAcademicDetails';
import ChangePassword from './components/ChangePassword';
import AddAcademicDetails from './components/AddAcademicDetails'
import GetAllStudents from './components/GetAllStudents';
import DisplayStudents from './components/DisplayStudents';
import DeleteStudent from './components/DeleteStudent';
import Admin from './components/Admin';
import UpdateAll from './components/UpdateAll';
import UpdateStudent from './components/UpdateStudent';
import GetAllRecords from './components/GetAllRecords';
import DisplayRecords from './components/DisplayRecords';
import Placement from './pages/Placement';
import SearchById from './components/CompanyComponents/SearchById';

import SearchByName from './components/CompanyComponents/SearchByName';
import AddCompany from './pages/Placement/AddCompany';
import GetAllCompany from './components/CompanyComponents/GetAllCompany';
import DisplayCompany from './components/CompanyComponents/DisplayCompany';
import Signup from './pages/Signup';
function App() {
  return (
    <div className="App">  
    <BrowserRouter>
      <Routes>
      <Route path="/signin" element={<Signin />} />
      <Route path="/home" element={<Home />}/>
      <Route path="/personalDetails" element={<PersonalDetails />}/>
      <Route path="/academicDetails" element={<AcademicDetails />}/>
      <Route path="/changePassword" element={<ChangePassword />}/>
      <Route path="/logout" element={<Logout />}/>
      <Route path="/faq" element={<Faq />}/>
      <Route path="/edit-details" element={<EditDetails />}/>
      <Route path="/edit-academicdetails" element={<EditAcademicDetails />}/>
      <Route path="/add-academicdetails" element={<AddAcademicDetails />}/>
      <Route path="/getAllStudents" element={<GetAllStudents />}/>
      <Route path="/displayStudents" element={<DisplayStudents />}/>
      <Route path="/delete-student" element={<DeleteStudent />}/>
      <Route path="/admin" element={<Admin />}/>
      <Route path="/updateAll" element={<UpdateAll />}/>
      <Route path="/update-student" element={<UpdateStudent />}/>
      <Route path="/getAllRecords" element={<GetAllRecords />}/>
      <Route path="/displayRecords" element={<DisplayRecords />}/>
      <Route path="/placement" element={<Placement />}/>
      <Route path="/searchById" element={<SearchById />}/>
      <Route path="/searchByName" element={<SearchByName />}/>
      <Route path="/addCompany" element={<AddCompany />}/>
      <Route path="/getAllCompany" element={<GetAllCompany />}/>
      <Route path="/displayCompany" element={<DisplayCompany />}/>
      <Route path="/signup" element={<Signup />}/>
      
      </Routes>
    </BrowserRouter>
    <ToastContainer theme="colored" />
    </div>
  )
}

export default App
