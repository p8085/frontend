import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as AiOutline from 'react-icons/ai';
import * as  BsFill  from "react-icons/bs";
import * as MdOutline from "react-icons/md";
import * as MdPlace from "react-icons/md";
import * as HiUserAdd from "react-icons/hi"
export const SidebarData = [
    {
      title: 'PersonalDetaills',
      path: '/personalDetails',
      icon: <AiIcons.AiFillHome />,
      cName: 'nav-text',
      spanCss:'leftAlign'
    },
    {
      title: 'AcademicDetails',
      path: '/academicDetails',
      icon: <IoIcons.IoMdPeople />,
      cName: 'nav-text',
      spanCss:'leftAlign'
    },
    {
        title: 'ChangePassword',
        path: '/changePassword',
        icon: <FaIcons.FaEnvelopeOpenText />,
        cName: 'nav-text',
        spanCss:'leftAlign'
    },
   {
        title: 'FAQ',
        path: '/faq',
        icon: <IoIcons.IoMdHelpCircle />,
        cName: 'nav-text',
        spanCss:'leftAlign'
    },
    {
      title: 'GetAllStudents',
      path: '/getAllStudents',
      icon: <BsFill.BsFillPersonBadgeFill />,
      cName: 'nav-text',
      spanCss:'leftAlign'
  }, 
 
{
  title: 'Get All Reords',
  path: '/getAllRecords',
  icon: <BsFill.BsFillPersonBadgeFill />,
  cName: 'nav-text',
  spanCss:'leftAlign'
},
{
  title: 'Placement',
  path: '/placement',
  icon: <MdPlace.MdPlace />,
  cName: 'nav-text',
  spanCss:'leftAlign'
},
{
  title: 'Add Company',
  path: '/addCompany',
  icon: <HiUserAdd.HiUserAdd />,
  cName: 'nav-text',
  spanCss:'leftAlign'
},
 {
  title: 'Logout',
  path: '/logout',
  icon: <AiOutline.AiOutlineLogout />,
  cName: 'nav-text',
  spanCss:'leftAlign'
}
];
  