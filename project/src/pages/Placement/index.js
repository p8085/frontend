import { useState } from 'react'
import Home from '../Home'
import axios from 'axios';
import SearchById from '../../components/CompanyComponents/SearchById';
import SearchByName from '../../components/CompanyComponents/SearchByName';
import { useNavigate } from 'react-router';
import GetAllCompany from '../../components/CompanyComponents/GetAllCompany';
import AddCompany from './AddCompany';
const styles = {
    space: {
        marginTop: '40px',
        marginLeft:'70px'
    }
}

const Placement = () => {
    const navigate = useNavigate()
    const [ id, setId ] = useState('')
  
    return (
        <div>
            <div>
                <Home />
            </div>
            
            <div className="row">
                <div className="col"></div>
                <div className="col">
                    <div class="input-group">
                        <input type="text" class="form-control" aria-label="Text input with segmented dropdown button" onChange={(e) => {
              setId(e.target.value)
            }}></input>
                            <button type="button" class="btn btn-outline-secondary">Search</button>
                            <button type="button" class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
                                <span class="visually-hidden">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-end">
                                <li><button class="dropdown-item" onClick={()=>{
                                    navigate('/searchById',{state : {id:id}})
                                }}>By Company Id</button></li>
                                <li><button class="dropdown-item" onClick={()=>{
                                    navigate('/searchByName',{state : {id:id}})
                                }}>By Company name</button></li>
                              
                            </ul>
                    </div>
                </div>
                <div className="col"></div>
            </div>
            <div style={styles.space}>
                <h4>Company Details:</h4>
            </div>
            <div style={styles.space}>
              <GetAllCompany />  
            </div>
            

        </div>
    )
}

export default Placement